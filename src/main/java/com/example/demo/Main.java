package com.example.demo;

public class Main {

    public static void main(String[] args) {

        final var main = new Main();
        main.update(new ConsoleLogger());
    }

    public void update(Logger logger) {

        logger.print("OMGHAI!");

        Item[] items = new Item[]{
                new Item("+5 Dexterity Vest", 10, 20), //
                new Item("Aged Brie", 2, 0), //
                new Item("Elixir of the Mongoose", 5, 7), //
                new Item("Sulfuras, Hand of Ragnaros", 0, 80), //
                new Item("Sulfuras, Hand of Ragnaros", -1, 80),
                new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20),
                new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49),
                new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49),
                // this conjured item does not work properly yet
                new Item("Conjured Mana Cake", 3, 6)};

        GildedRose app = new GildedRose(items);

        int days = 2;

        for (int i = 0; i < days; i++) {
            logger.print("-------- day " + i + " --------");

            logger.print("name, sellIn, quality");

            for (Item item : items) {
                logger.print(item.toString());
            }
            logger.print("\n");
            app.updateQuality();
        }
    }
}
