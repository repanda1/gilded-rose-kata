package com.example.demo;

public class ConsoleLogger implements Logger {

    @Override
    public void print(final String value) {
        System.out.println(value);
    }
}
