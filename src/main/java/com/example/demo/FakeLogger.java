package com.example.demo;

public class FakeLogger implements Logger {

    StringBuilder logs = new StringBuilder();

    @Override
    public void print(final String value) {
        logs.append(value);
        logs.append("\n");
    }

    public StringBuilder getLogs() {
        return logs;
    }
}
