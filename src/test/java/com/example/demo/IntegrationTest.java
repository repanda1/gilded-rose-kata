package com.example.demo;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

public class IntegrationTest {

    /**
     * see https://www.codurance.com/publications/2012/11/11/testing-legacy-code-with-golden-master
     */
    @Test
    public void golden_master_test() {
        final var main = new Main();
        final var logger = new FakeLogger();

        main.update(logger);

        Approvals.verify(logger.getLogs());
    }

}
