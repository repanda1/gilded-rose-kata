package com.example.demo;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    @Test
    void quality_of_an_item_is_never_negative() {
        Item[] items = new Item[]{new Item("foo", 0, 0)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("foo", app.items[0].name);
        assertEquals(-1, app.items[0].sellIn);
        assertEquals(0, app.items[0].quality);
    }

    @Test
    void normal_item1_Quality_degrades_by_one() {
        Item[] items = new Item[]{new Item("normal item", 1, 0)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("normal item", app.items[0].name);
        assertEquals(0, app.items[0].sellIn);
        assertEquals(0, app.items[0].quality);
    }

    @Test
    void Once_the_sell_by_date_has_passed_normal_item_Quality_degrades_twice_as_fast() {
        Item[] items = new Item[]{new Item("normal item", 0, 2)};
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertEquals("normal item", app.items[0].name);
        assertEquals(-1, app.items[0].sellIn);
        assertEquals(0, app.items[0].quality);
    }
}